#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - SYS - MAKEFILE
# By FIST Kresimir Mandic
#
#------------------------------------------------------------------------------
TD			= X:\sys\ 
AD          = Y:\sys\ 
YTD			= Y:\NewVer.RAR\sys\ 
COMD        = .\lib\syscommon.fle 
#------------------------------------------------------------------------------
# Compiler options
#------------------------------------------------------------------------------
CC          = start /w c:\vscd\bin\nova

CFLAGS      = -w -ereadfgl
#------------------------------------------------------------------------------
DEL			= del
COPY		= copy
CCOWN		= start /w D:\snova\sncd.mas\nova
OWN			= cr_fist_01_001
BRA			= cr_fist_01_101
#------------------------------------------------------------------------------

SYSFILES    = $(TD)sys_dd.fle $(TD)sys.fle $(TD)login.fle $(TD)syloadap.fle   \
	$(TD)useri.fle $(TD)appls.fle $(TD)progs.fle $(TD)tabele.fle  \
	$(TD)aplprava.fle $(TD)prgprava.fle $(TD)tabprava.fle $(TD)keyprava.fle   \
	$(TD)loginlog.fle $(TD)chkperms.fle $(TD)semafor.fle $(TD)syssemaf.fle    \
	$(TD)translog.fle $(TD)sellist.fle $(TD)l_progs.fle $(TD)sysdpd.fle       \
	$(TD)sysdisp.fle $(TD)l_aprava.fle $(TD)l_kprava.fle $(TD)l_pprava.fle    \
	$(TD)l_tprava.fle $(TD)l_useri.fle $(TD)l_tabs.fle $(TD)syspassw.fle      \
	$(TD)asci_sys.fle $(TD)sellist1.fle $(TD)sellist2.fle $(TD)clients.fle    \
	$(TD)printers.fle $(TD)cl_prns.fle $(TD)db_test.fle $(TD)grdefprn.fle     \
	$(TD)repsetup.fle $(TD)selrep.fle $(TD)r_useri.fle $(TD)presclib.fle      \
	$(TD)prn_esc.fle $(TD)rep_seq.fle $(TD)vr_dok.fle $(TD)del_knjig.fle       \
	$(TD)useri_cl.fle $(TD)sellist6.fle $(TD)snprompt.fle $(TD)cp_conv.fle    \
	$(TD)sysdptch.fle $(TD)dptchlib.fle $(TD)dpdummy.fle $(TD)syspars.fle     \
	$(TD)setsysdp.fle $(TD)fmsg.fle $(TD)dmenu.fle $(TD)actcllib.fle          \
	$(TD)activ_cl.fle $(TD)fistlib.fle $(TD)sntpl.fle $(TD)sntpl_ex.fle       \
	$(TD)fisticons.fle $(TD)delactcl.fle $(TD)aunlklog.fle $(TD)tspin.fle     \
	$(TD)grlib.fle $(TD)grfview.fle $(TD)snsrctrl.fle $(TD)snsrvcfg.fle       \
	$(TD)ps_mon.fle $(TD)ssclib.fle $(TD)freeze.fle $(TD)srvprocs.fle         \
	$(TD)sys_rmt.fle $(TD)bkp_kris.fle $(TD)asci_tab.fle $(TD)lasci_tab.fle   \
    $(TD)sys_a_dd.fle $(TD)sys_idx.fle $(TD)fpool.fle $(TD)sndba1.fle       \
    $(TD)lusr_prava.fle $(TD)lusr_list.fle $(TD)news.fle $(TD)sys_rev.fle   \
    $(TD)postavke.fle $(TD)fiskal_lib.fle $(TD)parametri.fle $(TD)hide_opts.fle 

# COMMON     =about.ls appls.ls asci_tab.ls bkp_kris.ls cp_conv.ls sellist6.ls \
            ## fpool.ls login.ls sellist.ls sndba1.ls fiskal_lib.ls tspin.ls \
            ## snprompt.ls sys_dd.ls sys_ddx.ls sysdpd.ls syspassw.ls tabele.ls \
            ## zmscmm.ls zmsmisc.ls zmstvbe.ls dptchlib.ls dpdummy.ls grlib.ls \
            ## syspars.ls setsysdp.ls ps_mon.ls fmsg.ls actcllib.ls chkperms.ls \
            ## semafor.ls dmenu.ls fistlib.ls selrep.ls presclib.ls 

ZMSTPLFILES  = $(TD)zmstvbe.fle $(TD)zmsmisc.fle $(TD)zmscmm.fle

all         : sys

sys     : $(SYSFILES) $(ZMSTPLFILES) $(COMMON)

################################################################

$(TD)zmstvbe.fle:  zmstvbe.lgc
    $(CC) $(CFLAGS) appl=zmstvbe dotfle=$(TD)zmstvbe.fle

$(TD)zmsmisc.fle:  zmsmisc.lgc
    $(CC) $(CFLAGS) appl=zmsmisc dotfle=$(TD)zmsmisc.fle

$(TD)zmscmm.fle:  zmscmm.lgc
    $(CC) $(CFLAGS) appl=zmscmm dotfle=$(TD)zmscmm.fle

### COMMON

fistlib.ls:  fistlib.lgc
    $(CC) $(CFLAGS) appl=fistlib dotfle=$(COMD)
    @echo " ">fistlib.ls

selrep.ls:  selrep.lgc
    $(CC) $(CFLAGS) appl=selrep dotfle=$(COMD)
    @echo " ">selrep.ls

presclib.ls:  presclib.lgc
    $(CC) $(CFLAGS) appl=presclib dotfle=$(COMD)
    @echo " ">presclib.ls

dmenu.ls:  dmenu.lgc
    $(CC) $(CFLAGS) appl=dmenu dotfle=$(COMD)
    @echo " ">dmenu.ls

chkperms.ls:  chkperms.lgc
    $(CC) $(CFLAGS) appl=chkperms dotfle=$(COMD)
    @echo " ">chkperms.ls

semafor.ls:  semafor.lgc
    $(CC) $(CFLAGS) appl=semafor dotfle=$(COMD)
    @echo " ">semafor.ls

actcllib.ls:  actcllib.lgc
    $(CC) $(CFLAGS) appl=actcllib dotfle=$(COMD)
    @echo " ">actcllib.ls

fmsg.ls:  fmsg.lgc
    $(CC) $(CFLAGS) appl=fmsg dotfle=$(COMD)
    @echo " ">fmsg.ls

ps_mon.ls:  ps_mon.lgc
    $(CC) $(CFLAGS) appl=ps_mon dotfle=$(COMD)
    @echo " ">ps_mon.ls

setsysdp.ls:  setsysdp.lgc
    $(CC) $(CFLAGS) appl=setsysdp dotfle=$(COMD)
    @echo " ">setsysdp.ls

syspassw.ls:  syspassw.lgc
    $(CC) $(CFLAGS) appl=syspassw dotfle=$(COMD)
    @echo " ">syspassw.ls

syspars.ls:  syspars.lgc
    $(CC) $(CFLAGS) appl=syspars dotfle=$(COMD)
    @echo " ">syspars.ls

grlib.ls:  grlib.lgc
    $(CC) $(CFLAGS) appl=grlib dotfle=$(COMD)
    @echo " ">grlib.ls

tspin.ls:  tspin.lgc
    $(CC) $(CFLAGS) appl=tspin dotfle=$(COMD)
    @echo " ">tspin.ls

dpdummy.ls:  dpdummy.lgc
    $(CC) $(CFLAGS) appl=dpdummy dotfle=$(COMD)
    @echo " ">dpdummy.ls

dptchlib.ls:  dptchlib.lgc
    $(CC) $(CFLAGS) appl=dptchlib dotfle=$(COMD)
    @echo " ">dptchlib.ls

fiskal_lib.ls:  fiskal_lib.lgc
    $(CC) $(CFLAGS) appl=fiskal_lib dotfle=$(COMD)
    @echo " ">fiskal_lib.ls

tabele.ls:  tabele.lgc
    $(CC) $(CFLAGS) appl=tabele dotfle=$(COMD)
    @echo " ">tabele.ls

zmscmm.ls:  zmscmm.lgc
    $(CC) $(CFLAGS) appl=zmscmm dotfle=$(COMD)
    @echo " ">zmscmm.ls

zmsmisc.ls:  zmsmisc.lgc
    $(CC) $(CFLAGS) appl=zmsmisc dotfle=$(COMD)
    @echo " ">zmsmisc.ls

zmstvbe.ls:  zmstvbe.lgc
    $(CC) $(CFLAGS) appl=zmstvbe dotfle=$(COMD)
    @echo " ">zmstvbe.ls

login.ls:  login.lgc
    $(CC) $(CFLAGS) appl=login dotfle=$(COMD)
    @echo " ">login.ls

sellist.ls:  sellist.lgc
    $(CC) $(CFLAGS) appl=sellist dotfle=$(COMD)
    @echo " ">sellist.ls

sellist6.ls:  sellist6.lgc
    $(CC) $(CFLAGS) appl=sellist6 dotfle=$(COMD)
    @echo " ">sellist6.ls

sndba1.ls:  sndba1.lgc
    $(CC) $(CFLAGS) appl=sndba1 dotfle=$(COMD)
    @echo " ">sndba1.ls

snprompt.ls:  snprompt.lgc
    $(CC) $(CFLAGS) appl=snprompt dotfle=$(COMD)
    @echo " ">snprompt.ls

sys_dd.ls:  sys_dd.lgc
    $(CC) $(CFLAGS) appl=sys_dd dotfle=.\lib\sys_dd.fle
    @echo " ">sys_dd.ls

sys_ddx.ls:  sys_ddx.lgc
    $(CC) $(CFLAGS) appl=sys_ddx dotfle=.\lib\sys_dd.fle
    @echo " ">sys_ddx.ls

sysdpd.ls:  sysdpd.lgc
    $(CC) $(CFLAGS) appl=sysdpd dotfle=$(COMD)
    @echo " ">sysdpd.ls


about.ls:  about.lgc
    $(CC) $(CFLAGS) appl=about dotfle=$(COMD)
    @echo " ">about.ls

appls.ls:  appls.lgc
    $(CC) $(CFLAGS) appl=appls dotfle=$(COMD)
    @echo " ">appls.ls

asci_tab.ls:  asci_tab.lgc
    $(CC) $(CFLAGS) appl=asci_tab dotfle=$(COMD)
    @echo " ">asci_tab.ls

bkp_kris.ls:  bkp_kris.lgc
    $(CC) $(CFLAGS) appl=bkp_kris dotfle=$(COMD)
    @echo " ">bkp_kris.ls

cp_conv.ls:  cp_conv.lgc
    $(CC) $(CFLAGS) appl=cp_conv dotfle=$(COMD)
    @echo " ">cp_conv.ls

fpool.ls:  fpool.lgc
    $(CC) $(CFLAGS) appl=fpool dotfle=$(COMD)
    @echo " ">fpool.ls

################################################################

$(TD)sys_dd.fle:  sys_dd.lgc
    $(CC) $(CFLAGS) appl=sys_dd dotfle=$(TD)sys_dd.fle
    $(CC) -w genddx ddfile=sys_dd
    $(CC) $(CFLAGS) appl=sys_ddx dotfle=$(TD)sys_dd.fle

$(TD)sys.fle:  sys.lgc
    $(CC) $(CFLAGS) appl=sys dotfle=$(TD)sys.fle

$(TD)login.fle:  login.lgc
    $(CC) $(CFLAGS) appl=login dotfle=$(TD)login.fle
    
$(TD)syloadap.fle:  syloadap.lgc
    $(CC) $(CFLAGS) appl=syloadap dotfle=$(TD)syloadap.fle
    
$(TD)useri.fle:  useri.lgc
    $(CC) $(CFLAGS) appl=useri dotfle=$(TD)useri.fle
    
$(TD)appls.fle:  appls.lgc
    $(CC) $(CFLAGS) appl=appls dotfle=$(TD)appls.fle
    
$(TD)progs.fle:  progs.lgc
    $(CC) $(CFLAGS) appl=progs dotfle=$(TD)progs.fle

$(TD)tabele.fle:  tabele.lgc
    $(CC) $(CFLAGS) appl=tabele dotfle=$(TD)tabele.fle

$(TD)vr_dok.fle:  vr_dok.lgc
    $(CC) $(CFLAGS) appl=vr_dok dotfle=$(TD)vr_dok.fle
    
$(TD)del_knjig.fle:  del_knjig.lgc
    $(CC) $(CFLAGS) appl=del_knjig dotfle=$(TD)del_knjig.fle
    
$(TD)aplprava.fle:  aplprava.lgc
    $(CC) $(CFLAGS) appl=aplprava dotfle=$(TD)aplprava.fle
    
$(TD)prgprava.fle:  prgprava.lgc
    $(CC) $(CFLAGS) appl=prgprava dotfle=$(TD)prgprava.fle
    
$(TD)tabprava.fle:  tabprava.lgc
    $(CC) $(CFLAGS) appl=tabprava dotfle=$(TD)tabprava.fle
    
$(TD)keyprava.fle:  keyprava.lgc
    $(CC) $(CFLAGS) appl=keyprava dotfle=$(TD)keyprava.fle
    
$(TD)loginlog.fle:  loginlog.lgc
    $(CC) $(CFLAGS) appl=loginlog dotfle=$(TD)loginlog.fle
    
$(TD)chkperms.fle:  chkperms.lgc
    $(CC) $(CFLAGS) appl=chkperms dotfle=$(TD)chkperms.fle
    
$(TD)semafor.fle:  semafor.lgc
    $(CC) $(CFLAGS) appl=semafor dotfle=$(TD)semafor.fle
    
$(TD)syssemaf.fle:  syssemaf.lgc
    $(CC) $(CFLAGS) appl=syssemaf dotfle=$(TD)syssemaf.fle
    
$(TD)translog.fle:  translog.lgc
    $(CC) $(CFLAGS) appl=translog dotfle=$(TD)translog.fle

$(TD)sellist.fle:  sellist.lgc
    $(CC) $(CFLAGS) appl=sellist dotfle=$(TD)sellist.fle
    
$(TD)l_progs.fle:  l_progs.lgc
    $(CC) $(CFLAGS) appl=l_progs dotfle=$(TD)l_progs.fle

$(TD)sysdpd.fle:  sysdpd.lgc
    $(CC) $(CFLAGS) appl=sysdpd dotfle=$(TD)sysdpd.fle
        
$(TD)sysdisp.fle:  sysdisp.lgc
    $(CC) $(CFLAGS) appl=sysdisp dotfle=$(TD)sysdisp.fle

$(TD)l_aprava.fle:  l_aprava.lgc
    $(CC) $(CFLAGS) appl=l_aprava dotfle=$(TD)l_aprava.fle

$(TD)l_kprava.fle:  l_kprava.lgc
    $(CC) $(CFLAGS) appl=l_kprava dotfle=$(TD)l_kprava.fle

$(TD)l_pprava.fle:  l_pprava.lgc
    $(CC) $(CFLAGS) appl=l_pprava dotfle=$(TD)l_pprava.fle

$(TD)l_tprava.fle:  l_tprava.lgc
    $(CC) $(CFLAGS) appl=l_tprava dotfle=$(TD)l_tprava.fle

$(TD)l_useri.fle:  l_useri.lgc
    $(CC) $(CFLAGS) appl=l_useri dotfle=$(TD)l_useri.fle
    
$(TD)l_tabs.fle:  l_tabs.lgc
    $(CC) $(CFLAGS) appl=l_tabs dotfle=$(TD)l_tabs.fle

$(TD)syspassw.fle:  syspassw.lgc
    $(CC) $(CFLAGS) appl=syspassw dotfle=$(TD)syspassw.fle

$(TD)asci_sys.fle:  asci_sys.lgc
    $(CC) $(CFLAGS) appl=asci_sys dotfle=$(TD)asci_sys.fle

$(TD)sellist1.fle:  sellist1.lgc
    $(CC) $(CFLAGS) appl=sellist1 dotfle=$(TD)sellist1.fle

$(TD)sellist2.fle:  sellist2.lgc
    $(CC) $(CFLAGS) appl=sellist2 dotfle=$(TD)sellist2.fle

$(TD)clients.fle:  clients.lgc
    $(CC) $(CFLAGS) appl=clients dotfle=$(TD)clients.fle

$(TD)printers.fle:  printers.lgc
    $(CC) $(CFLAGS) appl=printers dotfle=$(TD)printers.fle

$(TD)cl_prns.fle:  cl_prns.lgc
    $(CC) $(CFLAGS) appl=cl_prns dotfle=$(TD)cl_prns.fle

$(TD)db_test.fle:  db_test.lgc
    $(CC) $(CFLAGS) appl=db_test dotfle=$(TD)db_test.fle

$(TD)grdefprn.fle:  grdefprn.lgc
    $(CC) $(CFLAGS) appl=grdefprn dotfle=$(TD)grdefprn.fle

$(TD)repsetup.fle:  repsetup.lgc
    $(CC) $(CFLAGS) appl=repsetup dotfle=$(TD)repsetup.fle

$(TD)selrep.fle:  selrep.lgc
    $(CC) $(CFLAGS) appl=selrep dotfle=$(TD)selrep.fle

$(TD)r_useri.fle:  r_useri.lgc
    $(CC) $(CFLAGS) appl=r_useri dotfle=$(TD)r_useri.fle

$(TD)presclib.fle:  presclib.lgc
    $(CC) $(CFLAGS) appl=presclib dotfle=$(TD)presclib.fle

$(TD)prn_esc.fle:  prn_esc.lgc
    $(CC) $(CFLAGS) appl=prn_esc dotfle=$(TD)prn_esc.fle

$(TD)rep_seq.fle:  rep_seq.lgc
    $(CC) $(CFLAGS) appl=rep_seq dotfle=$(TD)rep_seq.fle

$(TD)useri_cl.fle:  useri_cl.lgc
    $(CC) $(CFLAGS) appl=useri_cl dotfle=$(TD)useri_cl.fle

$(TD)sellist6.fle:  sellist6.lgc
    $(CC) $(CFLAGS) appl=sellist6 dotfle=$(TD)sellist6.fle

$(TD)snprompt.fle:  snprompt.lgc
    $(CC) $(CFLAGS) appl=snprompt dotfle=$(TD)snprompt.fle

$(TD)cp_conv.fle:  cp_conv.lgc
    $(CC) $(CFLAGS) appl=cp_conv dotfle=$(TD)cp_conv.fle

$(TD)sysdptch.fle:  sysdptch.lgc
    $(CC) $(CFLAGS) appl=sysdptch dotfle=$(TD)sysdptch.fle
               
$(TD)dptchlib.fle:  dptchlib.lgc
    $(CC) $(CFLAGS) appl=dptchlib dotfle=$(TD)dptchlib.fle
               
$(TD)dpdummy.fle:  dpdummy.lgc
    $(CC) $(CFLAGS) appl=dpdummy dotfle=$(TD)dpdummy.fle
               
$(TD)syspars.fle:  syspars.lgc
    $(CC) $(CFLAGS) appl=syspars dotfle=$(TD)syspars.fle

$(TD)setsysdp.fle:  setsysdp.lgc
    $(CC) $(CFLAGS) appl=setsysdp dotfle=$(TD)setsysdp.fle

$(TD)fmsg.fle:  fmsg.lgc
    $(CC) $(CFLAGS) appl=fmsg dotfle=$(TD)fmsg.fle

$(TD)dmenu.fle:  dmenu.lgc
    $(CC) $(CFLAGS) appl=dmenu dotfle=$(TD)dmenu.fle

$(TD)actcllib.fle:  actcllib.lgc
    $(CC) $(CFLAGS) appl=actcllib dotfle=$(TD)actcllib.fle

$(TD)activ_cl.fle:  activ_cl.lgc
    $(CC) $(CFLAGS) appl=activ_cl dotfle=$(TD)activ_cl.fle

$(TD)fistlib.fle:  fistlib.lgc
    $(CC) $(CFLAGS) appl=fistlib dotfle=$(TD)fistlib.fle

$(TD)sntpl.fle:  sntpl.lgc
    $(CC) $(CFLAGS) appl=sntpl dotfle=$(TD)sntpl.fle

$(TD)sntpl_ex.fle:  sntpl_ex.lgc
    $(CC) $(CFLAGS) appl=sntpl_ex dotfle=$(TD)sntpl_ex.fle

$(TD)fisticons.fle:  fisticons.lgc
    $(CC) $(CFLAGS) appl=fisticons dotfle=$(TD)fisticons.fle

$(TD)delactcl.fle:  delactcl.lgc
    $(CC) $(CFLAGS) appl=delactcl dotfle=$(TD)delactcl.fle

$(TD)aunlklog.fle:  aunlklog.lgc
    $(CC) $(CFLAGS) appl=aunlklog dotfle=$(TD)aunlklog.fle

$(TD)tspin.fle:  tspin.lgc
    $(CC) $(CFLAGS) appl=tspin dotfle=$(TD)tspin.fle

$(TD)grlib.fle:  grlib.lgc
    $(CC) $(CFLAGS) appl=grlib dotfle=$(TD)grlib.fle

$(TD)grfview.fle:  grfview.lgc
    $(CC) $(CFLAGS) appl=grfview dotfle=$(TD)grfview.fle

$(TD)snsrctrl.fle:  snsrctrl.lgc
    $(CC) $(CFLAGS) appl=snsrctrl dotfle=$(TD)snsrctrl.fle

$(TD)snsrvcfg.fle:  snsrvcfg.lgc
    $(CC) $(CFLAGS) appl=snsrvcfg dotfle=$(TD)snsrvcfg.fle

$(TD)ps_mon.fle:  ps_mon.lgc
    $(CC) $(CFLAGS) appl=ps_mon dotfle=$(TD)ps_mon.fle

$(TD)ssclib.fle:  ssclib.lgc
    $(CC) $(CFLAGS) appl=ssclib dotfle=$(TD)ssclib.fle

$(TD)freeze.fle:  freeze.lgc
    $(CC) $(CFLAGS) appl=freeze dotfle=$(TD)freeze.fle

$(TD)srvprocs.fle:  srvprocs.lgc
    $(CC) $(CFLAGS) appl=srvprocs dotfle=$(TD)srvprocs.fle

$(TD)sys_rmt.fle:  sys_rmt.lgc
    $(CC) $(CFLAGS) appl=sys_rmt dotfle=$(TD)sys_rmt.fle

$(TD)bkp_kris.fle:  bkp_kris.lgc
    $(CC) $(CFLAGS) appl=bkp_kris dotfle=$(TD)bkp_kris.fle

$(TD)asci_tab.fle:  asci_tab.lgc
    $(CC) $(CFLAGS) appl=asci_tab dotfle=$(TD)asci_tab.fle
    $(CC) $(CFLAGS) appl=asci_tab dotfle=$(AD)asci_tab.fle

$(TD)lasci_tab.fle:  lasci_tab.lgc
    $(CC) $(CFLAGS) appl=lasci_tab dotfle=$(TD)lasci_tab.fle
    $(CC) $(CFLAGS) appl=lasci_tab dotfle=$(AD)sys.fle

$(TD)sys_a_dd.fle:  sys_a_dd.lgc
    $(CC) $(CFLAGS) appl=sys_a_dd dotfle=$(TD)sys_a_dd.fle

$(TD)sndba1.fle:  sndba1.lgc
    $(CC) $(CFLAGS) appl=sndba1 dotfle=$(TD)sndba1.fle
    $(CC) $(CFLAGS) appl=sndba1 dotfle=$(AD)sndba1.fle

$(TD)fpool.fle:  fpool.lgc
    $(CC) $(CFLAGS) appl=fpool dotfle=$(TD)fpool.fle
    $(CC) $(CFLAGS) appl=fpool dotfle=$(AD)sndba1.fle

$(TD)sys_idx.fle:  sys_idx.lgc
    $(CC) $(CFLAGS) appl=sys_idx dotfle=$(TD)sys_idx.fle

$(TD)lusr_prava.fle:  lusr_prava.lgc
    $(CC) $(CFLAGS) appl=lusr_prava dotfle=$(TD)lusr_prava.fle

$(TD)lusr_list.fle:  lusr_list.lgc
    $(CC) $(CFLAGS) appl=lusr_list dotfle=$(TD)lusr_list.fle

$(TD)news.fle:  news.lgc
    $(CC) $(CFLAGS) appl=news dotfle=$(TD)news.fle

$(TD)sys_rev.fle:  sys_rev.lgc
    $(CC) $(CFLAGS) appl=sys_rev dotfle=$(TD)sys_rev.fle
    COPY sys_rev.lgc $(YTD)sys_rev.lgc

$(TD)postavke.fle:  postavke.lgc
    $(CC) $(CFLAGS) appl=postavke dotfle=$(TD)postavke.fle

$(TD)fiskal_lib.fle:  fiskal_lib.lgc
    $(CC) $(CFLAGS) appl=fiskal_lib dotfle=$(TD)fiskal_lib.fle

$(TD)parametri.fle:  parametri.lgc
    $(CC) $(CFLAGS) appl=parametri dotfle=$(TD)parametri.fle

$(TD)hide_opts.fle:  hide_opts.lgc
    $(CC) $(CFLAGS) appl=hide_opts dotfle=$(TD)hide_opts.fle

