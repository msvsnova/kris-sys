--USE [PRESS]
GO
/****** Object:  User [snova]    Script Date: 09/09/2015 15:22:52 ******/
DROP USER [snova]
GO
CREATE USER [snova] FOR LOGIN [snova]
GO
EXEC sp_addrolemember N'db_owner', N'snova'
GO
