#-------------------------------------------------------------------------------
#
# Check for environment parameter RWPAPERSIZE. This will only be done once !
# NOTE : Paper format 'Letter' is default (standard USA format)
#
#-------------------------------------------------------------------------------

FUNCTION getValue vRepPaperSize
{
    HASVALUE vRepPaperSize
    TEST BASE              vRepPaperSize != ''
ALT
    SET vPaperSizeParameter	toupper(parameter('RWPAPERSIZE'), 1, -1)
    {
        # IF old env.parameter RWPAPERSIZE has not been set AND the printer
        # is an MS-Windows printer
        # THEN use the new zero-sized output medium (i.e. engine will first 
        # ask the printer for its current page settings)
        #
        # This means that the pre-5.1.5 situation can be achieved by setting 
        # the environment parameter RWPAPERSIZE.
        #
        TEST BASE          vPaperSizeParameter == '' && \
        			vRepPrinterCategory == 'MS-Windows'
        SET vRepPaperSize  'MS-Windows'
    ALT
        TEST BASE          find(vPaperSizeParameter, 'LEGAL') == 'LEGAL'
        SET vRepPaperSize  'Legal'
    ALT
        TEST BASE          find(vPaperSizeParameter, 'A4') == 'A4'
        SET vRepPaperSize  'A4'
    ALT
        TEST BASE          find(vPaperSizeParameter, 'EXEC') == 'EXEC'
        SET vRepPaperSize  'Execut'
    ALT
        SET vRepPaperSize  'Letter'
    }
    continue {
        TEST BASE          find(vPaperSizeParameter, 'LSC') == 'LSC' || \
                           find(vPaperSizeParameter, 'LANDSC') == 'LANDSC'

        SET vRepPaperSize  strip(vRepPaperSize)+'Lsc'
    }
}
FUNCTION configure reportPrinter
{
    EXEC BASE              'TYPE '+strip(vRepPaperSize)+\
                           'Printer strip(vRepPrinterCategory)'

    EXEC BASE              toupper(vRepFileOrCommand,1,-1)+' '+\
                           strip(vRepPaperSize)+\
                           'Printer strip(vRepFileOrCmdname)'

    EXEC BASE              'PREVIEW '+strip(vRepPaperSize)+\
                           'Printer strip(vRepViewBoolean)'
}
#________________________________________________________________________________
#
# Paper          Physical Size    Printable Area   Printable Area in points
#________________________________________________________________________________
#
# LetterPrinter  8.5 x 11 in.     7.9 x 10.6 in.   (201*72)/25.4 x (255*72)/25.4
# LegalPrinter   8.5 x 14 in.     7.9 x 13.6 in.   (201*72)/25.4 x (332*72)/25.4
# A4Printer      210 x 297 mm     195 x 273 mm     (195*72)/25.4 x (273*72)/25.4
# ExecutPrinter  7.25 x 10.5 in.  6.75 x 10.1 in.  (169*72)/25.4 x (243*72)/25.4



#===============================================================================
# inch to points :   multiply with 72
# mm to inch :       divide by 25.4
#
# As a result :
# mm to points :     first multiply with 72, followed by a division by 25.4 
# (in that order ; to make sure that we do not lose significant digits !!)
#===============================================================================


# MS-Windows printer which uses current page setup of the printer.
# This behaviour can be forced by setting both pagewidth and pageheight to zero.
# NOTE: you can use Control-Panel under Windows to change page setup.
#
OUTPUT MS-WindowsPrinter     WIDTH=0            HEIGHT=0	PAPERSIZEUNIT=pts

# Letter paper format :
#
OUTPUT LetterPrinter       WIDTH=569            HEIGHT=722	PAPERSIZEUNIT=pts
OUTPUT LetterLscPrinter   HEIGHT=569             WIDTH=722	PAPERSIZEUNIT=pts

# Legal paper format :
#
OUTPUT LegalPrinter        WIDTH=569            HEIGHT=941	PAPERSIZEUNIT=pts
OUTPUT LegalLscPrinter    HEIGHT=569             WIDTH=941	PAPERSIZEUNIT=pts

# A4 paper format :
#
OUTPUT A4Printer           WIDTH=552            HEIGHT=773	PAPERSIZEUNIT=pts
OUTPUT A4LscPrinter       HEIGHT=552             WIDTH=773	PAPERSIZEUNIT=pts

# Executive paper format :
#
OUTPUT ExecutPrinter       WIDTH=479            HEIGHT=688	PAPERSIZEUNIT=pts
OUTPUT ExecutLscPrinter   HEIGHT=479             WIDTH=688	PAPERSIZEUNIT=pts
#
#

FIELD vRepPrinterCategory  string(76)           STATIC
FIELD vRepViewBoolean      string(3)            STATIC
FIELD vRepFileOrCommand    string(10)           STATIC
FIELD vRepFileOrCmdname    string(256)          STATIC
FIELD vRepViewTitle        string(100)          STATIC

FIELD vRepPaperSize        string(10)           STATIC
FIELD vPaperSizeParameter  string(20)

FUNCTION version xrep {
   FMSG BASE '$$ xrep, version 1.0.9.1, Aug 19 1998'
}
